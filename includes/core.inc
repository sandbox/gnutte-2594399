<?php

function drupal_config_copy_core_config_info() {
    return array(
        'weigth' => 0
    );
}

function drupal_config_copy_core_config_list(&$data) {
    $data['modules'] = array();
    
    $modules = system_list('module_enabled');
    foreach ($modules as $module => $description) {
        $data['modules'][] = $module;
    }
    
    return true;
}

function drupal_config_copy_core_config_get(&$data) {
    $data['modules'] = array();
    
    $modules = system_list('module_enabled');
    $data['modules'] = $modules;
    
    return true;
}

function drupal_config_copy_core_config_diff($origin, $full=FALSE) {
    $set = $diff = array();
    !$full ? drupal_config_copy_core_config_list($set) : drupal_config_copy_core_config_get($set);
    
    $origin_modules = array_keys($origin['modules']);
    $set_modules = array_keys($set['modules']);
    
    $diff['modules'] = array('plus' => array(), 'minus' => array());
    $diff['modules']['plus'] = array_diff($origin_modules, $set_modules);
    $diff['modules']['minus'] = array_diff($set_modules, $origin_modules);
    $diff['modules']['update'] = array();
    
    return $diff;
}

function drupal_config_copy_core_config_diff_apply($origin) {
    $diff = drupal_core_config_core_config_diff($origin, TRUE);
    
    module_enable($diff['modules']['plus']);
    module_disable($diff['modules']['minus']);
    
    return TRUE;
}