<?php

function drupal_config_copy_block_config_info() {
    return array(
        'weigth' => 0
    );
}

function drupal_config_copy_block_config_list(&$data) {
    $blocks = module_invoke_all('block_info');
    $data['block'] = array_keys($blocks);
            
    return true;
}

function drupal_config_copy_block_config_get(&$data) {
    $blocks = module_invoke_all('block_info');
    $data['block'] = $blocks;

    return true;
}

function drupal_config_copy_block_config_diff($origin, $full = FALSE) {
    $set = $diff = array();
    !$full ? drupal_config_copy_block_config_list($set) : drupal_config_copy_block_config_get($set);

    $origin_block = array_keys($origin['block']);
    $set_block = array_keys($set['block']);
    
    $diff['block'] = array('plus' => array(), 'minus' => array());
    $diff['block']['plus'] = array_diff($origin_block, $set_block);
    $diff['block']['minus'] = array_diff($set_block, $origin_block);
    $diff['block']['update'] = array();

    return $diff;
}

function drupal_config_copy_block_config_diff_apply($origin) {
    $diff = drupal_block_config_block_config_diff($origin, TRUE);


    return TRUE;
}
