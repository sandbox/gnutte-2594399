<?php

function drupal_config_copy_taxonomy_config_info() {
    return array(
        'weigth' => 1
    );
}

function drupal_config_copy_taxonomy_config_list(&$data) {
    $data['taxonomy'] = array();
    
    return TRUE;
}

function drupal_config_copy_taxonomy_config_get(&$data) {
    $data['taxonomy'] = array();
    
    return TRUE;
}

function drupal_config_copy_taxonomy_config_diff($origin, $full=FALSE) {
    $set = $diff = array();
    !$full ? drupal_config_copy_taxonomy_config_list($set) : drupal_config_copy_taxonomy_config_get($set);
    
    $origin_taxonomy = array_keys($origin['taxonomy']);
    $set_taxonomy = array_keys($set['taxonomy']);
    
    $diff['taxonomy'] = array('plus' => array(), 'minus' => array());
    $diff['taxonomy']['plus'] = array_diff($origin_taxonomy, $set_taxonomy);
    $diff['taxonomy']['minus'] = array_diff($set_taxonomy, $origin_taxonomy);
    $diff['taxonomy']['update'] = array();
    
    return $diff;
}