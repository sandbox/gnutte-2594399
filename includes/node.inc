<?php

function drupal_config_copy_node_config_info() {
    return array(
        'weigth' => 1
    );
}

function drupal_config_copy_node_config_list(&$data) {
    $data['types'] = array();
    
    $types = node_type_get_types();
    foreach ($types as $type) {
        $data['types'][] = $type->type;
    }
    
    return TRUE;
}

function drupal_config_copy_node_config_get(&$data) {
    $data['types'] = node_type_get_types();
    
    return TRUE;
}

function drupal_config_copy_node_config_diff($origin, $full=FALSE) {
    $set = $diff = array();
    !$full ? drupal_config_copy_node_config_list($set) : drupal_config_copy_node_config_get($set);
    
    $origin_node_types = array_keys($origin['types']);
    $set_node_types = array_keys($set['types']);
    
    $diff['types'] = array('plus' => array(), 'minus' => array());
    $diff['types']['plus'] = array_diff($origin_node_types, $set_node_types);
    $diff['types']['minus'] = array_diff($set_node_types, $origin_node_types);
    $diff['types']['update'] = array();
    
    return $diff;
}