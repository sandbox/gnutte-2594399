<?php

function drupal_config_copy_field_config_info() {
    return array(
        'weigth' => 2,
        'require' => array(
            'node',
            'taxonomy'
        )
    );
}

function drupal_config_copy_field_config_list(&$data) {
    $data['fields'] = $data['instances'] = array();
    
    $fields = field_info_fields();
    foreach ($fields as $name => $field) {
        $data['fields'][] = $name;
    }
    
    $instances = field_info_instances();
    foreach ($instances as $entity_type => $entity_instances) {
        $data['instances'][$entity_type] = array();
        
        foreach ($entity_instances as $bundle => $field_instances) {
            $data['instances'][$entity_type][$bundle] = array();
            
            foreach ($field_instances as $field_name => $field_instance) {
                $data['instances'][$entity_type][$bundle][] = $field_name;
            }
        }
    }
}

function drupal_config_copy_field_config_get(&$data) {
    $data['fields'] = $data['instances'] = array();
    $data['fields'] = field_info_fields();
    $data['instances'] = field_info_instances();
    
    return TRUE;
}

function drupal_config_copy_field_config_diff($origin, $full=FALSE) {
    $set = $diff = array();
    !$full ? drupal_config_copy_field_config_list($set) : drupal_config_copy_field_config_get($set);
    
    $origin_fields = array_keys($origin['fields']);
    $set_fields = array_keys($set['fields']);
    
    $diff['fields'] = array('plus' => array(), 'minus' => array());
    $diff['fields']['plus'] = array_diff($origin_fields, $set_fields);
    $diff['fields']['minus'] = array_diff($set_fields, $origin_fields);
    $diff['fields']['update'] = array();
    
    return $diff;
}