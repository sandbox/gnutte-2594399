<?php

function drupal_config_copy_process_init(&$context) {
    variable_set('maintenance_mode', 1);
}

function drupal_config_copy_process_build_config($origin, $module, &$context) {
    extract($origin);

    $result = drupal_http_request($url . "/drupal/copy/" . $token . "/" . $module . "/config/list");
    if (is_object($result) && isset($result->data)) {
        $data = drupal_json_decode($result->data);

        switch ($data['status']) {
            case 'error':
                drupal_set_message(t($data['message']), 'error');
                return FALSE;
            case 'warning':
                drupal_set_message(t($data['message']), 'warning');
                return TRUE;
            case 'success':
                //On importe les données
                module_load_include('inc', 'drupal_config_copy', 'includes/' . $module);

                $function_name = 'drupal_config_copy_' . $module . '_config_diff_apply';
                if (!function_exists($function_name)) {
                    drupal_set_message(t("The function does not exist."), 'error');
                    return FALSE;
                }

                if (!$function_name($data)) {
                    drupal_set_message(t("The function for config diff in module !module set an error.", array('!module' => $module)), 'error');
                    return FALSE;
                }
                break;
        }
    }

    return FALSE;
}

function drupal_config_copy_process_diff_config($origin, $module, &$context) {
    extract($origin);
    
    $result = drupal_http_request($url . "/drupal/copy/" . $token . "/" . $module . "/config/get");
    if (is_object($result) && isset($result->data)) {
        $data = drupal_json_decode($result->data);

        switch ($data['status']) {
            case 'error':
                drupal_set_message(t($data['message']), 'error');
                return FALSE;
            case 'warning':
                drupal_set_message(t($data['message']), 'warning');
                return TRUE;
            case 'success':
                module_load_include('inc', 'drupal_config_copy', 'includes/' . $module);
                
                $function_name = "drupal_config_copy_".$module."_config_diff";
                if(function_exists($function_name)) {
                    $context['results'][$module] = $function_name($data, TRUE);
                }
                break;
        }
    }
    
    return FALSE;
}

function drupal_config_copy_process_end($success, $results, $operations) {
    drupal_set_message("<pre>".print_r($results, true)."</pre>");
    
    variable_set('maintenance_mode', 0);
}
