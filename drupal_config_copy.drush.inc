<?php

function drupal_config_copy_drush_command() {
    $items = array();
    
    $items['copy-run'] = array(
        'callback' => 'drupal_config_copy_drush_run',
        'description' => dt('Run configuration copy between two drupal'),
        'aliases' => array('cr'),
        'examples' => array(
            'drush copy-run' => 'Run a configuration copy'
        ),
        'arguments' => array()
    );
    
    return $items;
}

function drupal_config_copy_drush_help($section) {
    switch($section) {
        case 'drush:copy-run':
            return dt("Run a configuration copy");
    }
}

function drupal_config_copy_drush_run() {
    
}