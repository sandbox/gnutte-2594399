<?php

function drupal_config_copy_settings_form($form, $form_state) {
    $form['origin'] = array('#tree' => true);
    $form['origin']['url'] = array('#type' => 'textfield', '#title' => t('URL'), '#required' => true);
    $form['origin']['token'] = array('#type' => 'textfield', '#title' => t('Token'), '#required' => true, '#default_value' => 'test');
    
    $form['diff'] = array('#type' => 'submit', '#value' => t('Diff'), '#submit' => array('drupal_config_copy_settings_form_diff_submit'));
    $form['import'] = array('#type' => 'submit', '#value' => t('Import'), '#submit' => array('drupal_config_copy_settings_form_import_submit'));
    
    return $form;
}

function drupal_config_copy_settings_form_validate($form, $form_state) {
    $values = $form_state['values'];
    
    if(drupal_config_copy_check_version($values['origin']['url']) != drupal_config_copy_check_version()) {
        form_set_error('url', t('The copy module version is different from the template site.'));
        return ;
    }
}

function drupal_config_copy_settings_form_import_submit($form, $form_state) {
    $values = $form_state['values'];
    
    $batch = array(
        'operations' => array(
            array('drupal_config_copy_process_build_config', array($values['origin'], 'core'))
        ),
        'finished' => 'drupal_config_copy_process_end',
        'title' => t('Running drupal configuration copy'),
        'init_message' => t('Drupal configuration copy is starting'),
        'progress_message' => t('Processed @current out of @total.'),
        'error_message' => t('Drupal configuration copy is in error.'),
        'file' => drupal_get_path('module', 'drupal_config_copy') . '/drupal_config_copy.batch.inc'
    );
    
    $infos = array();
    foreach ($infos as $module) {
        $batch['operations'][] = array('drupal_config_copy_process_build_config', array($values['origin'], $module));
    }
    
    batch_set($batch);
    
    batch_process('/admin/structure/drupal-copy');
}

function drupal_config_copy_settings_form_diff_submit($form, $form_state) {
    $values = $form_state['values'];
    
    $batch = array(
        'operations' => array(
            array('drupal_config_copy_process_diff_config', array($values['origin'], 'core'))
        ),
        'finished' => 'drupal_config_copy_process_end',
        'title' => t('Running drupal configuration diff'),
        'init_message' => t('Drupal configuration diff is starting'),
        'progress_message' => t('Processed @current out of @total.'),
        'error_message' => t('Drupal configuration diff is in error.'),
        'file' => drupal_get_path('module', 'drupal_config_copy') . '/drupal_config_copy.batch.inc'
    );
    
    $infos = array('block');
    foreach ($infos as $module) {
        $batch['operations'][] = array('drupal_config_copy_process_diff_config', array($values['origin'], $module));
    }
    
    batch_set($batch);
    
    batch_process('/admin/structure/drupal-copy');
}